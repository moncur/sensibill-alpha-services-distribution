# Sensibill Alpha Services

Sensibill Alpha Services are built using React native to dynamically push new alpha feature to an app without requiring a new app build. 

_______________________________
### Alpha Features 

* Insight Tiles
	* Monthly Spend Tracker
	* Cuisine Spend
	* Spend Breakdown
	* Tax Tile

#### Insight Tiles 

##### Monthly Spend Tracker 

* Curious as to how much your morning avo toast habit is adding up to? Set budgets for various food and drink items and track your spending habits at Restaurants & Cafes.

##### Cuisine Spend 

* How varied are your culinary tastes? Use this tile to track your top cuisine spend at Restaurants & Cafes.

##### Spend Breakdown 

* Take a deep dive into your monthly spend. Drill down into folders or categories to see where your money is going.

##### Tax Tile 

* Is your small business registered for GST/HST? Track the taxes you've paid on expenses and estimate your potential ITC credit. 

_______________________________
### Installation Guide 

#### Preparation 

Ensure that you have the __SensibillSDK__ included in your project

#### Add Frameworks 

0. Download __SensibillPilot.framework__ and __Instabug.framework__ from the repository

0. Drag frameworks in your __Xcode__ project

0. Remove __SensibillPilot__ and __Instabug__ from __Linked Frameworks and Libraries__ and add both to __Embedded Binaries__. You should now each framework in both locations. 

![embedded frameworks][embedded frameworks]

0. Under __Build Settings__ set __Always Embed Swift Standard Libraries__ to _YES_

![always embed][always embed]

#### Wrapping Code 

Create a singleton class called `PilotManager` that initializes the framework and addes the floating action button to Receipt List and Receipt Detail screen. 


	#import "PilotManager.h"
	#import "SensibillSDK.h"
	#import "SBSDKBundle.h"
	#import "SBReceiptListController.h"
	#import "SBReceiptDetailViewController.h"
	#import "SBSessionLocalStore.h"

	@import SensibillPilot;

	@interface PilotManager ()

	@property (strong, nonatomic) SBPilotFloatingMenuController *receiptListPilotMenuController;
	@property (strong, nonatomic) SBPilotFloatingMenuController *receiptDetailsPilotMenuController;

	@end

	@implementation PilotManager

	+ (instancetype)sharedInstance {
		static id sharedInstance = nil;
		@synchronized ([self class]) {
			if (sharedInstance == nil) {
				sharedInstance = [[self class] new];
			}
			return sharedInstance;
		}
	}

	- (void)setup {
		[SensibillPilotCore setupWithAccess:SBSessionLocalStore.accessToken forEnvironment:SBPilotEnvironmentBetaProduction completion:^(BOOL success, NSError *error) {
			if (success) {
				//TODO: handle success case
			} else {
				//TODO: handle error case
				NSLog(@"%@", error.localizedDescription);
			}
		}];

		// Add Pilot UI to receipt list
		[SBReceiptListController addViewDidLoadBlock:^(UIViewController *viewController) {
			self.receiptListPilotMenuController = [[SBPilotFloatingMenuController alloc] initWithRootViewController:viewController screenName:@"receipt-list"];
		} withName:@"PilotBlock"];

		[SBReceiptDetailViewController addViewDidLoadBlock:^(UIViewController *viewController, SBReceipt *receipt) {
			self.receiptDetailsPilotMenuController = [[SBPilotFloatingMenuController alloc] initWithRootViewController:viewController screenName:@"receipt-detail"];
			self.receiptDetailsPilotMenuController.receiptID = receipt.ID;
		} withName:@"PilotBlock"];
	}

	@end


[always embed]:              https://bytebucket.org/moncur/sensibill-alpha-services-distribution/raw/eadba28975739bfb96d85b65f946bd788d8141e3/img/always-embed-swift.png
[embedded frameworks]:       https://bytebucket.org/moncur/sensibill-alpha-services-distribution/raw/eadba28975739bfb96d85b65f946bd788d8141e3/img/embedded-frameworks.png